import sympy as sym
import math
import numpy
from scipy.spatial import cKDTree
from collections import OrderedDict




def intersection(list,number):

    print('- - - Sound Localization using TDOA values - - -')

    devices=[]
    equation_variables=[]
    hyperbolae=[]
    points=[]
    hyperbola_graphs=[]
    possible=[]
    testH=[]
    testK=[]
    testA=[]
    testB=[]
    result=[]
    solution_set=[]
    noDup=[]

    def haversine (newH, newK, h, k):
        rLatA = math.radians(h);
        rLatB = math.radians(newH);
        dLat = math.radians(newH-h);
        dLong = math.radians(newK-k);

        a= math.sin(dLat/2)*math.sin(dLat/2)+math.cos(rLatA)*math.cos(rLatB)*math.sin(dLong/2)*math.sin(dLong/2);

        c = 2*math.atan2(math.sqrt(a),math.sqrt(1-a));

        A = 6371000*c;

        return A


    def hyperbola(deviceA,deviceB):


        scale=0.000009

        latA = deviceA[0]
        longA = deviceA[1]
        timeA = deviceA[2]

        latB = deviceB[0]
        longB = deviceB[1]
        timeB = deviceB[2]

        tdoa=abs(deviceA[2]-deviceB[2])

        distance_difference=(tdoa/1000)*343.21

        print('DISTANCE DIFF')
        print(distance_difference)

        h=(deviceA[0]+deviceB[0])/2
        k=(deviceA[1]+deviceB[1])/2

        print('H')
        print(h)
        print('K')
        print(k)

        a=distance_difference/2
        print('BEFORE IS')
        print(a)
        print('AFTER is')
        a*=scale
        print(a)

        slope=(deviceB[1]-deviceA[1])/(deviceB[0]-deviceA[0])

        print('SLOPE IS')
        print(slope)

        deg=numpy.arctan((slope))
        deg=math.degrees(deg)

        #offset px
        px = h+a*math.sqrt(1/(1+(slope*slope)));
        py = k+a*slope*math.sqrt(1/(1+(slope*slope)));

        print('PX')
        print(px)
        print('PY')
        print(py)

        newLatitude = px
        newLongitude = py

        A=(haversine(newLatitude,newLongitude, h,k))*scale
        print('A:')
        print(A)
        C = (haversine(deviceB[0],deviceB[1],h,k))*scale
        print('C:')
        print(C)
        B = (math.sqrt((C*C)-(A*A)));
        print('B:')
        print(B)
        print('DEG:')
        print(deg)
        return h,k,A,B,deg


    for x in range (0,number):
        lat=float(list[x][0])
        long=float(list[x][1])
        time=float(list[x][2])
        device_details = [lat,long,time]
        devices.append(device_details)

        print(devices)

    for x in range(0,number):
        if x==number-1:
            h,k,a,b,angle = hyperbola(devices[0],devices[x])
            variables=[h,k,a,b,angle]
            equation_variables.append(variables)
            testH.append(h)
            testK.append(k)
            testA.append(a)
            testB.append(b)
        else:
            h,k,a,b,angle = hyperbola(devices[x],devices[x+1])
            variables=[h,k,a,b,angle]
            equation_variables.append(variables)
            testH.append(h)
            testK.append(k)
            testA.append(a)
            testB.append(b)


    for i in equation_variables:
        x,y=sym.symbols('x,y')

        #hyperbola=((((x-i[0])**2)/(i[2]*i[2]))-(((y-i[1])**2)/(i[3]*i[3]))-1.)
        hyperbola=(((((y-i[1])*math.sin(i[4])+(x-i[0])*math.cos(i[4]))*((y-i[1])*math.sin(i[4])+(x-i[0])*math.cos(i[4])))/(i[2]*i[2]) - (((y-i[1])*math.cos(i[4])-(x-i[0])*math.sin(i[4]))*((y-i[1])*math.cos(i[4])-(x-i[0])*math.sin(i[4])))/(i[3]*i[3]))-1.)
        hyperbolae.append(hyperbola)

    length=len(hyperbolae)

    for i in range (0,length):
        #points.append("_\n")
        #if i==len(hyperbolae)-1:
        #   points.append(sym.solve([hyperbolae[0],hyperbolae[i]],[x,y]))
        #else:
        #   points.append(sym.solve([hyperbolae[i],hyperbolae[i+1]],[x,y]))
        if i==len(hyperbolae)-1:
            try:
                points.append(sym.nsolve([hyperbolae[0],hyperbolae[i]],[x,y],[testH[i],testK[i]],solver='halley'))
            except:
                print("Something went wrong")
            try:
                points.append(sym.nsolve([hyperbolae[0],hyperbolae[i]],[x,y],[testH[i]+testA[i],testK[i]+testB[i]],solver='halley'))
            except:
                print("Something went wrong")
            try:
                points.append(sym.nsolve([hyperbolae[0],hyperbolae[i]],[x,y],[testH[i]-testA[i],testK[i]-testB[i]],solver='halley'))
            except:
                print("Something went wrong")
            try:
                points.append(sym.nsolve([hyperbolae[0],hyperbolae[i]],[x,y],[testH[i]+testA[i],testK[i]-testB[i]],solver='halley'))
            except:
                print("Something went wrong")
            try:
                points.append(sym.nsolve([hyperbolae[0],hyperbolae[i]],[x,y],[testH[i]-testA[i],testK[i]+testB[i]],solver='halley'))
            except:
                print("Something went wrong")

        else:
            try:
                points.append(sym.nsolve([hyperbolae[i],hyperbolae[i+1]],[x,y],[testH[i],testK[i]],solver='halley'))
            except:
                print("Something went wrong")
            try:
                points.append(sym.nsolve([hyperbolae[i],hyperbolae[i+1]],[x,y],[testH[i]+testA[i],testK[i]+testB[i]],solver='halley'))
            except:
                print("Something went wrong")
            try:
                points.append(sym.nsolve([hyperbolae[i],hyperbolae[i+1]],[x,y],[testH[i]-testA[i],testK[i]-testB[i]],solver='halley'))
            except:
                print("Something went wrong")
            try:
                points.append(sym.nsolve([hyperbolae[i],hyperbolae[i+1]],[x,y],[testH[i]+testA[i],testK[i]-testB[i]],solver='halley'))
            except:
                print("Something went wrong")
            try:
                points.append(sym.nsolve([hyperbolae[i],hyperbolae[i+1]],[x,y],[testH[i]-testA[i],testK[i]+testB[i]],solver='halley'))
            except:
                print("Something went wrong")
        solution_set.append(points)

        points=[]
        #points.append("_\n")

    # Remove duplicates
    temp=[]
    #final=[]
    #print(solution_set)
    for set in solution_set:
        #i = list(dict.fromkeys(i))
        #print(set)
        #print("BUTTOCKS")
        #print(solution_set)
        for i in set:
            #print("YAPPOOO")
            #print(i)
            if i not in temp:
                print(i)
                temp.append(i)
        noDup.append(temp)
        temp=[]

        #list(OrderedDict.fromkeys(i))
        print("NO DUPLICATES")
        print(noDup)
    print('s')
    print(noDup[0])
    print(noDup[1])
    print(noDup[2])
    print(len(noDup))
    print('end')
    min=999999
    index_j=0
    index_k=0
    final=[]

    for j in noDup[0]:
        for k in noDup[1]:
            x2=(k[0]-j[0])*(k[0]-j[0])
            y2=(k[1]-j[1])*(k[1]-j[1])
            d=math.sqrt((x2+y2)*1.0)

            if d<min:
                min=d
                index_j=j
                index_k=k
    #COMPARE WITHIN
    if len(noDup[0])>1:
        j=noDup[0][0]
        k=noDup[0][1]
        x2=(k[0]-j[0])*(k[0]-j[0])
        y2=(k[1]-j[1])*(k[1]-j[1])
        d=math.sqrt((x2+y2)*1.0)

        if d<min:
            min=d
            index_j=j
            index_k=k

        if len(noDup[1])>1:
            j=noDup[1][0]
            k=noDup[1][1]
            x2=(k[0]-j[0])*(k[0]-j[0])
            y2=(k[1]-j[1])*(k[1]-j[1])
            d=math.sqrt((x2+y2)*1.0)

            if d<min:
                min=d
                index_j=j
                index_k=k
###############END

    final.append(index_j)
    final.append(index_k)

    min=999999
    index_a=0
    index_b=0

    # for j in noDup[2]:
    #     print(final[0][1])
    #
    #     x2=(final[0][0]-j[0])*(final[0][0]-j[0])
    #     y2=(final[0][1]-j[1])*(final[0][1]-j[1])
    #     d=math.sqrt((x2+y2)*1.0)
    #
    #     if d<min:
    #         min=d
    #         index_a=j

        # final.append(index_a)
    for j in noDup[1]:
        for k in noDup[2]:
            x2=(k[0]-j[0])*(k[0]-j[0])
            y2=(k[1]-j[1])*(k[1]-j[1])
            d=math.sqrt((x2+y2)*1.0)

            if d<min:
                min=d
                index_j=j
                index_k=k

    #COMPARE WITHIN
    if len(noDup[1])>1:
        j=noDup[1][0]
        k=noDup[1][1]
        x2=(k[0]-j[0])*(k[0]-j[0])
        y2=(k[1]-j[1])*(k[1]-j[1])
        d=math.sqrt((x2+y2)*1.0)

        if d<min:
            min=d
            index_j=j
            index_k=k

        if len(noDup[2])>1:
            j=noDup[2][0]
            k=noDup[2][1]
            x2=(k[0]-j[0])*(k[0]-j[0])
            y2=(k[1]-j[1])*(k[1]-j[1])
            d=math.sqrt((x2+y2)*1.0)

            if d<min:
                min=d
                index_j=j
                index_k=k
    ###############END

    final.append(index_j)
    final.append(index_k)

    min=999999
    index_a=0
    index_b=0

    for j in noDup[0]:
        for k in noDup[2]:
            x2=(k[0]-j[0])*(k[0]-j[0])
            y2=(k[1]-j[1])*(k[1]-j[1])
            d=math.sqrt((x2+y2)*1.0)

            if d<min:
                min=d
                index_j=j
                index_k=k

    #COMPARE WITHIN
    if len(noDup[0])>1:
        j=noDup[0][0]
        k=noDup[0][1]
        x2=(k[0]-j[0])*(k[0]-j[0])
        y2=(k[1]-j[1])*(k[1]-j[1])
        d=math.sqrt((x2+y2)*1.0)

        if d<min:
            min=d
            index_j=j
            index_k=k

        if len(noDup[2])>1:
            j=noDup[2][0]
            k=noDup[2][1]
            x2=(k[0]-j[0])*(k[0]-j[0])
            y2=(k[1]-j[1])*(k[1]-j[1])
            d=math.sqrt((x2+y2)*1.0)

            if d<min:
                min=d
                index_j=j
                index_k=k
    ###############END

    final.append(index_j)
    final.append(index_k)


    print("final")
    print(final)

    # for j in noDup[2]:
    #
    #     for k in final[1]:
    #         x2=(k[0]-j[0])*(k[0]-j[0])
    #         y2=(k[1]-j[1])*(k[1]-j[1])
    #         d=math.sqrt((x2+y2)*1.0)
    #
    #         if d<min:
    #             min_b=d
    #             index_b=j




    for a in final:
        print(final)
        #result.append("Pair Result\n")
        for b in a:
            #possible.append(b)
            result.append(b)
        #result.append("___________\n")
    return result;


# def intersection(list,number):
#
#     return list;
#
#
#     x,y=sym.symbols('x,y')
#
#     # h_a=1
#     # k_a=2
#     # a_a=16
#     # b_a=4
#     #
#     # h_b=2
#     # k_b=7
#     # a_b=9
#     # b_b=4
#
#     hyperbola_a = ((((x-h_a)**2)/a_a)-(((y-k_a)**2)/b_a)-1.)
#     hyperbola_b = ((((x-h_b)**2)/a_b)-(((y-k_b)**2)/b_b)-1.)
#
#     return sym.solve([hyperbola_a,hyperbola_b],[x,y])