/*
 * Copyright (C) 2012 Jacquet Wong
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * musicg api in Google Code: http://code.google.com/p/musicg/
 * Android Application in Google Play: https://play.google.com/store/apps/details?id=com.whistleapp
 *
 */

package com.musicg.demo.android;

import java.util.LinkedList;

import android.media.AudioFormat;
import android.media.AudioRecord;

import com.musicg.api.ClapApi;
import com.musicg.wave.WaveHeader;

public class DetectorThread extends Thread{

	private RecorderThread recorder;
	private WaveHeader waveHeader;
	private ClapApi clapApi;
	private volatile Thread _thread;

	private LinkedList<Boolean> clapResultList = new LinkedList<Boolean>();
	private int numClaps;
	private int clapCheckLength = 1;
	private int clapPassScore = 1;

	private long myCurrentTimeMillis;

	private OnSignalsDetectedListener onSignalsDetectedListener;

	public DetectorThread(RecorderThread recorder){
		this.recorder = recorder;
		AudioRecord audioRecord = recorder.getAudioRecord();

		int bitsPerSample = 0;
		if (audioRecord.getAudioFormat() == AudioFormat.ENCODING_PCM_16BIT){
			bitsPerSample = 16;
		}
		else if (audioRecord.getAudioFormat() == AudioFormat.ENCODING_PCM_8BIT){
			bitsPerSample = 8;
		}

		int channel = 0;
		// clap detection only supports mono channel
		//if (audioRecord.getChannelConfiguration() == AudioFormat.CHANNEL_CONFIGURATION_MONO){
			channel = 1;
		//}

		waveHeader = new WaveHeader();
		waveHeader.setChannels(channel);
		waveHeader.setBitsPerSample(bitsPerSample);
		waveHeader.setSampleRate(audioRecord.getSampleRate());
		clapApi = new ClapApi(waveHeader);
	}

	private void initBuffer() {
		numClaps = 0;
		clapResultList.clear();

		// init the first frames
		for (int i = 0; i < clapCheckLength; i++) {
			clapResultList.add(false);
		}
		// end init the first frames
	}

	public void start() {
		_thread = new Thread(this);
		_thread.start();
	}

	public void stopDetection(){
		_thread = null;
	}

	public void run() {
		try {
			byte[] buffer;
			initBuffer();

			Thread thisThread = Thread.currentThread();
			while (_thread == thisThread) {
				// detect sound
				buffer = recorder.getFrameBytes();

				// audio analyst
				if (buffer != null) {
					// sound detected
					// clap detection
					//System.out.println("Sound Detected");
					boolean isClap = clapApi.isClap(buffer);
					if (clapResultList.getFirst()) {
						numClaps--;
					}

					clapResultList.removeFirst();
					clapResultList.add(isClap);

					if (isClap) {
						numClaps++;
						System.out.println("CLAP");
						initBuffer();
						onClapDetected();
						//Time
//						myCurrentTimeMillis = System.currentTimeMillis();
//						System.out.println(myCurrentTimeMillis);
					}
					//System.out.println("num:" + numClaps);

					if (numClaps >= clapPassScore) {
						// clear buffer
//						initBuffer();
//						onClapDetected();
					}
					// end clap detection
				}
				else{
					// no sound detected
					if (clapResultList.getFirst()) {
						numClaps--;
					}
					clapResultList.removeFirst();
					clapResultList.add(false);
				}
				// end audio analyst
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void onClapDetected(){
		if (onSignalsDetectedListener != null){
			onSignalsDetectedListener.onClapDetected();
		}
	}

	public void setOnSignalsDetectedListener(OnSignalsDetectedListener listener){
		onSignalsDetectedListener = listener;
	}
}