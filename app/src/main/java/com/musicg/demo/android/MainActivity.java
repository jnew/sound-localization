/*
 * Copyright (C) 2012 Jacquet Wong
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * musicg api in Google Code: http://code.google.com/p/musicg/
 * Android Application in Google Play: https://play.google.com/store/apps/details?id=com.whistleapp
 *
 */

package com.musicg.demo.android;


import com.chaquo.python.*;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.instacart.library.truetime.TrueTimeRx;

import android.util.Log;
import android.widget.Toast;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static android.content.ContentValues.TAG;


public class MainActivity extends Activity implements OnSignalsDetectedListener {
	private LocationManager locationManager;
	private LocationListener locationListener;

	static MainActivity mainApp;

	public static final int DETECT_NONE = 0;
	public static final int DETECT_WHISTLE = 1;
	public static int selectedDetection = DETECT_NONE;

	// detection parameters
	private DetectorThread detectorThread;
	private RecorderThread recorderThread;
	private int numWhistleDetected = 0;

	// views
	private View mainView, listeningView, wifiView;
	private Button whistleButton;

	private double longitude;
	private double latitude;
	private long time;
	private long myCurrentTimeMillis;
	Location location;
	boolean initialLoc = true;
	boolean initialTime = true;

	double previousReceiveTime;

	Date trueTime;

	//Buttons
	Button btnOnOff, btnDiscover, btnSend;
	ListView listView;
	TextView read_msg_box, connectionStatus;
	EditText writeMsg;

	WifiManager wifiManager;
	WifiP2pManager mManager;
	WifiP2pManager.Channel mChannel;

	BroadcastReceiver mReceiver;
	IntentFilter mIntentFilter;

	List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
	String[] deviceNameArray;
	WifiP2pDevice[] deviceArray;

	static final int MESSAGE_READ = 1;

	ServerClass serverClass;
	ClientClass clientClass;
	SendReceive sendReceive;

	boolean first = false;
	int numberOfNodes = 1;
	ArrayList<double[]> devices = new ArrayList<double[]>();

	WifiP2pInfo myInfo = null;

    List<PyObject> answers = new ArrayList<PyObject>();

//	double[][] deviceList = new double[3][3];
	String[][] deviceList = new String[3][3];

	double timeRecorded;

	String mDevice, previousDevice=null;

	boolean doneCalculating = true;

	int participatingNodes = 1;

	String myTime;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Clap Recognition");

		mainApp = this;

		initializeValues();

		// set views
		LayoutInflater inflater = LayoutInflater.from(this);
		mainView = inflater.inflate(R.layout.startup, null);
		listeningView = inflater.inflate(R.layout.listening, null);
		wifiView = inflater.inflate(R.layout.wifiview, null);
		setContentView(mainView);

		//Location
		locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
		locationListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location loc) {
				if (initialLoc) {
					location = loc;
					initialLoc = false;
					System.out.println("IM REAAAAAAAAAAAAADY");
					locationUpdate();
				} else {
					if (loc.getAccuracy() > location.getAccuracy())
						location = loc;
				}
			}

			@Override
			public void onStatusChanged(String s, int i, Bundle bundle) {

			}

			@Override
			public void onProviderEnabled(String s) {

			}

			@Override
			public void onProviderDisabled(String s) {

			}
		};

//		if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
//		    return;
//        }


		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);




		TrueTimeRx.build().initializeRx("time.google.com")
				.subscribeOn(Schedulers.io())
				.subscribe(date -> {
							if (!TrueTimeRx.isInitialized()) {
							Log.d(TAG, "Sorry TrueTime not yet initialized");
							return;
						}
						//trueTime=date;
					Date trueTime = TrueTimeRx.now();
					System.out.println("TIME IS AGAIN: " +TrueTimeRx.now().getTime());
						initialTime=false;
						timeUpdate();
				}, throwable -> {
					Log.e(TAG, "TrueTime init failed: ", throwable);
				});

	}

	Handler handler = new Handler(new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what){
				case MESSAGE_READ:
					byte[] readBuff= (byte[]) msg.obj;
					String tempMsg = new String(readBuff, 0, msg.arg1);
//					read_msg_box.setText(tempMsg);
					read_msg_box.append("\n\nReceived\n" +tempMsg);



					double timeSent;


					//Split the String first
					String[] splitted = tempMsg.split(",");
					String senderName = splitted[3];
					timeSent = Double.parseDouble(splitted[2]);

					if(!senderName.equals(previousDevice) && participatingNodes<=numberOfNodes) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						if (Math.abs(timeSent - timeRecorded) < 2000) {

							participatingNodes++;
							read_msg_box.append("\n\n---Recognized---\n" +tempMsg);

							//Assign to a var
							if (participatingNodes == 2) {
								deviceList[1][0] = splitted[0];
								deviceList[1][1] = splitted[1];
								deviceList[1][2] = splitted[2];
								read_msg_box.append("\nObtained node 1\n");
								previousReceiveTime = timeSent;
							} else if (participatingNodes == 3) {
								if (Math.abs(timeSent - previousReceiveTime) < 1500) {
									deviceList[2][0] = splitted[0];
									deviceList[2][1] = splitted[1];
									deviceList[2][2] = splitted[2];
									read_msg_box.append("\nObtained node 2\n");
									read_msg_box.append("\n\nCalcuating...\n");
									doneCalculating = false;

									read_msg_box.append("\n\nHere are the values: \n");
									read_msg_box.append("HOST\n");
									read_msg_box.append(deviceList[0][0] + "\n");
									read_msg_box.append(deviceList[0][1] + "\n");
									read_msg_box.append(deviceList[0][2] + "\n");
									read_msg_box.append(deviceList[1][0] + "\n");
									read_msg_box.append(deviceList[1][1] + "\n");
									read_msg_box.append(deviceList[1][2] + "\n");
									read_msg_box.append(deviceList[2][0] + "\n");
									read_msg_box.append(deviceList[2][1] + "\n");
									read_msg_box.append(deviceList[2][2] + "\n");

									calculate();
								}
								else{
									deviceList[2][0] = splitted[0];
									deviceList[2][1] = splitted[1];
									deviceList[2][2] = splitted[2];
									read_msg_box.append("\nObtained node 1 - retried\n");
									previousReceiveTime = timeSent;
								}
							} else if (participatingNodes == 4) {
								if (Math.abs(timeSent - previousReceiveTime) < 1500) {
									deviceList[1][0] = splitted[0];
									deviceList[1][1] = splitted[1];
									deviceList[1][2] = splitted[2];
									read_msg_box.append("\nObtained node 1 again\n");
									read_msg_box.append("\n\nCalcuating...\n");
									doneCalculating = false;

									read_msg_box.append("\n\nHere are the values: \n");
									read_msg_box.append("HOST\n");
									read_msg_box.append(deviceList[0][0] + "\n");
									read_msg_box.append(deviceList[0][1] + "\n");
									read_msg_box.append(deviceList[0][2] + "\n");
									read_msg_box.append(deviceList[1][0] + "\n");
									read_msg_box.append(deviceList[1][1] + "\n");
									read_msg_box.append(deviceList[1][2] + "\n");
									read_msg_box.append(deviceList[2][0] + "\n");
									read_msg_box.append(deviceList[2][1] + "\n");
									read_msg_box.append(deviceList[2][2] + "\n");

									calculate();
								}
							}


						} else {
							// Reset number of participating nodes
							participatingNodes = 2;
							deviceList[1][0] = splitted[0];
							deviceList[1][1] = splitted[1];
							deviceList[1][2] = splitted[2];
						}
						previousDevice = splitted[3];
					}
					else{

						if (Math.abs(timeSent - timeRecorded) > 2000) {
							participatingNodes = 2;
							deviceList[1][0] = splitted[0];
							deviceList[1][1] = splitted[1];
							deviceList[1][2] = splitted[2];

							read_msg_box.append("\nObtained node 1\n");
							previousReceiveTime = timeSent;
							previousDevice = splitted[3];
						}
					}

					break;
			}
			return true;
		}
	});

	private void calculate(){
		read_msg_box.append("\n\n" +deviceList +"\n");

		//Calling a python source
		Python py = Python.getInstance();

		try {
			PyObject hyp = py.getModule("hyperbola_intersection");

            answers = hyp.callAttr("intersection",deviceList, 3).asList();

            read_msg_box.append("\n\nPossible Location Acquired");

            for(PyObject p : answers){
                read_msg_box.append("\n\n" +p);
            }

		}catch(Exception e){
			read_msg_box.append("\n\n" +"Exception occured\nToo much error in time\n");
		}catch(Error e2){
			read_msg_box.append("\n\n" +"Error occured!\n");
		}



		doneCalculating = true;
		participatingNodes = 1;
		//numberOfNodes=1;

	}

	private void exqListener(){
		btnOnOff.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(wifiManager.isWifiEnabled()){
					wifiManager.setWifiEnabled(false);
				}
				else{
					wifiManager.setWifiEnabled(true);
				}
			}
		});

		btnDiscover.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
					@Override
					public void onSuccess() {
						connectionStatus.setText("Discovery Started");
					}

					@Override
					public void onFailure(int i) {
						connectionStatus.setText("Discovery Failed");
					}
				});
			}
		});

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				final WifiP2pDevice device = deviceArray[i];
				WifiP2pConfig config = new WifiP2pConfig();
				config.deviceAddress = device.deviceAddress;

				mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {
                //mManager.createGroup(mChannel, new WifiP2pManager.ActionListener() {
					@Override
					public void onSuccess() {
						Toast.makeText(getApplicationContext(),"Connected to "+device.deviceName,Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onFailure(int i) {
						Toast.makeText(getApplicationContext(),"Not Connected", Toast.LENGTH_SHORT).show();
					}
				});
			}
		});

//		btnSend.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View view) {
//				String msg = writeMsg.getText().toString();
//				sendReceive.write(msg.getBytes());
//			}
//		});
	}

	private void initialWork(){
		btnOnOff=(Button) findViewById(R.id.onOff);
		btnDiscover=(Button) findViewById(R.id.discover);
		//btnSend=(Button) findViewById(R.id.sendButton);
		listView=(ListView) findViewById(R.id.peerListView);
		read_msg_box=(TextView) findViewById(R.id.readMsg);
		connectionStatus=(TextView) findViewById(R.id.connectionStatus);
		//writeMsg=(EditText) findViewById(R.id.writeMsg);

		read_msg_box.setMovementMethod(new ScrollingMovementMethod());

//		wifiManager= (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//		mManager= (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
//		//mManager = getSys
//		mChannel = mManager.initialize(this,getMainLooper(),null);
//
//		mReceiver = new WiFiDirectBroadcastReceiver(mManager,mChannel,this);
//		mIntentFilter = new IntentFilter();
//		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
//		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
//		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
//		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

	}

	protected void initializeValues(){
		wifiManager= (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		mManager= (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
		//mManager = getSys
		mChannel = mManager.initialize(this,getMainLooper(),null);

		mReceiver = new WiFiDirectBroadcastReceiver(mManager,mChannel,this);

		/////YAHP KUHAON KO AK DEVICE NAME
		mDevice = ((WiFiDirectBroadcastReceiver) mReceiver).getMyDevice();

		mIntentFilter = new IntentFilter();
		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
		mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

		mDevice=android.os.Build.MODEL;
	}

	WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener() {
		@Override
		public void onPeersAvailable(WifiP2pDeviceList peerList) {
			if(!peerList.getDeviceList().equals(peers)){
				peers.clear();
				peers.addAll(peerList.getDeviceList());

				deviceNameArray = new String[peerList.getDeviceList().size()];
				deviceArray = new WifiP2pDevice[peerList.getDeviceList().size()];
				int index=0;

				for(WifiP2pDevice device : peerList.getDeviceList()){
					deviceNameArray[index] = device.deviceName;
					deviceArray[index] = device;
					index++;
				}

				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,deviceNameArray);
				try {
					listView.setAdapter(adapter);
				}catch(Exception e){

				}
			}

			if(peers.size()==0){
				Toast.makeText(getApplicationContext(),"No Device Found", Toast.LENGTH_SHORT).show();
				return;
			}
		}
	};

	WifiP2pManager.ConnectionInfoListener connectionInfoListener = new WifiP2pManager.ConnectionInfoListener() {
		@Override
		public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {
			final InetAddress groupOwnerAddress = wifiP2pInfo.groupOwnerAddress;



			if(wifiP2pInfo.groupFormed && wifiP2pInfo.isGroupOwner){
				try {
					connectionStatus.setText("Host");
					myInfo = wifiP2pInfo;
				}catch(Exception e){

				}
				if(first==false) {
					serverClass = new ServerClass();
					serverClass.start();
					listenNow();
				}

			}
			else if(wifiP2pInfo.groupFormed){
				try {
					connectionStatus.setText("Client");
					myInfo = wifiP2pInfo;
				}catch(Exception e){

				}
				clientClass = new ClientClass(groupOwnerAddress);
				clientClass.start();
				listenNow();
			}
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(mReceiver,mIntentFilter);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(mReceiver);
	}

	public class ServerClass extends Thread{
		Socket socket;
		ServerSocket serverSocket;

		@Override
		public void run() {
			try {
//				do{
					serverSocket = new ServerSocket(8888);
				do{	socket = serverSocket.accept();
					numberOfNodes++;
					sendReceive  =new SendReceive(socket);
					sendReceive.start();
				}while(true);
//				serverSocket = new ServerSocket(8888);
//				socket = serverSocket.accept();
//				sendReceive  =new SendReceive(socket);
//				sendReceive.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private class SendReceive extends Thread{
		private Socket socket;
		private InputStream inputStream;
		private OutputStream outputStream;

		public SendReceive(Socket skt){
			socket = skt;
			try {
				inputStream = socket.getInputStream();
				outputStream = socket.getOutputStream();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			byte[] buffer = new byte[1024];
			int bytes;

			while(socket!=null){
				try {
					bytes = inputStream.read(buffer);
					if(bytes>0){
						handler.obtainMessage(MESSAGE_READ,bytes,-1, buffer).sendToTarget();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		public void write(final byte[] bytes){
//			try {
				//outputStream.write(bytes);

                new Thread(new Runnable(){

                    @Override
                    public void run()
                    {
                        try
                        {
                            outputStream.write(bytes);
                        }
                        catch (IOException e)
                        {e.printStackTrace();}
                    }}).start();

//			} catch (IOException e) {
//				e.printStackTrace();
//			}
		}
	}

	public class ClientClass extends Thread{
		Socket socket;
		String hostAdd;

		public ClientClass(InetAddress hostAddress){
			hostAdd=hostAddress.getHostAddress();
			socket = new Socket();
		}

		@Override
		public void run() {
			try {
				socket.connect(new InetSocketAddress(hostAdd,8888),500);
				sendReceive = new SendReceive(socket);
				sendReceive.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void goHomeView() {
		setContentView(mainView);
		if (recorderThread != null) {
			recorderThread.stopRecording();
			recorderThread = null;
		}
		if (detectorThread != null) {
			detectorThread.stopDetection();
			detectorThread = null;
		}
		selectedDetection = DETECT_NONE;
	}

	private void goListeningView(){
		setContentView(listeningView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "Quit demo");
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			finish();
			break;
		default:
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			goHomeView();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	class ClickEvent implements OnClickListener {
		public void onClick(View view) {
			if (view == whistleButton) {
				selectedDetection = DETECT_WHISTLE;
				recorderThread = new RecorderThread();
				recorderThread.start();
				detectorThread = new DetectorThread(recorderThread);
				detectorThread.setOnSignalsDetectedListener(MainActivity.mainApp);
				detectorThread.start();
				goListeningView();
			}
		}
	}

	public void listenNow(){
		//setContentView(listeningView);
		selectedDetection = DETECT_WHISTLE;
		recorderThread = new RecorderThread();
		recorderThread.start();
		detectorThread = new DetectorThread(recorderThread);
		detectorThread.setOnSignalsDetectedListener(MainActivity.mainApp);
		detectorThread.start();
	}

	public void searchDevices(){
		setContentView(wifiView);
		initialWork();
		exqListener();
	}

	protected void onDestroy() {
		super.onDestroy();
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public void locationUpdate() {
		runOnUiThread(new Runnable() {
			public void run() {
				TextView longitudeView = (TextView) MainActivity.mainApp.findViewById(R.id.locationReady);
				longitudeView.setText("Ready!");
				if(initialTime==false){
					//listenNow();
					searchDevices();
				}
			}
		});
	}

    public void timeUpdate() {
        runOnUiThread(new Runnable() {
            public void run() {
                TextView timeUpdateView = (TextView) MainActivity.mainApp.findViewById(R.id.timeReady);
                timeUpdateView.setText("Time updated!");
                if(initialLoc==false){
					//listenNow();
					searchDevices();
				}

            }
        });
    }

	@Override
	public void onClapDetected() {

		String timeDetected=String.valueOf(TrueTimeRx.now().getTime());

		runOnUiThread(new Runnable() {
			public void run() {

			    if(location!=null && myInfo!=null && !myInfo.isGroupOwner && doneCalculating) {

//			    	String text = "Time is: " +String.valueOf(TrueTimeRx.now().getTime()) +"\n Longitude: " +location.getLongitude()
//							+"\n Latitude: " +location.getLatitude();
//					String text = "Time is: " +String.valueOf(timeDetected) +"\n Longitude: " +location.getLongitude()
//							+"\n Latitude: " +location.getLatitude();


//			    	String info = location.getLatitude()+","+location.getLongitude()+","+String.valueOf(TrueTimeRx.now().getTime()+","+mDevice);
//					String info = location.getLongitude()+","+location.getLatitude()+","+String.valueOf(TrueTimeRx.now().getTime()+","+mDevice);
					String info = location.getLongitude()+","+location.getLatitude()+","+timeDetected+","+mDevice;

					read_msg_box.append("\n\n---Recognized---\n" +info);

					sendReceive.write(info.getBytes());


                }

				if(location!=null && myInfo!=null && myInfo.isGroupOwner && doneCalculating) {

					if(Math.abs(timeRecorded - Double.parseDouble(String.valueOf(timeDetected))) > 2000){
						deviceList[0][1] = ""+location.getLatitude();
						deviceList[0][0] = ""+location.getLongitude();
						deviceList[0][2] = timeDetected;
						myTime=timeDetected;

						String info = location.getLongitude() + "," + location.getLatitude() + "," + myTime + "," + mDevice;
						read_msg_box.append("\n\nHost : ClapDetected\n" + info);

						timeRecorded=Double.parseDouble(myTime);
					}


				}
			}
		});
	}
}
